(cd "~/")
;;=======================================================
;; ▼ 必須の基本設定
;;=======================================================

;; メニューバー ツールバー スクロールバーを非表示
;; スタートアップメッセージを表示させない
;; *scratch*の初期メッセージ削除
(menu-bar-mode 0)
(tool-bar-mode 0)
(scroll-bar-mode -1)
(setq inhibit-startup-message 1)
(setq initial-scratch-message "

______________________________________________________________________________


      _|_|_|    _|_|_|  _|_|_|      _|_|    _|_|_|_|_|    _|_|_|  _|    _|
    _|        _|        _|    _|  _|    _|      _|      _|        _|    _|
      _|_|    _|        _|_|_|    _|_|_|_|      _|      _|        _|_|_|_|
          _|  _|        _|    _|  _|    _|      _|      _|        _|    _|
    _|_|_|      _|_|_|  _|    _|  _|    _|      _|        _|_|_|  _|    _|

                         C-j : open-junk-file
______________________________________________________________________________
")

;; モードラインからモード一覧を消す
(delete 'mode-line-modes mode-line-format)

;; タイトルにフルパス表示
(setq frame-title-format "%f")

;; 対応する括弧をハイライト
(show-paren-mode 1)

;; カーソルの点滅をやめる
(blink-cursor-mode 0)

;; マウススクロールは1行ごとに
;; マウススクロールの加速をやめる
(setq mouse-wheel-scroll-amount '(1 ((shift) . 5)))
(setq mouse-wheel-progressive-speed nil)

;; C-nで1行ずつスクロール
(setq scroll-conservatively 35
      scroll-margin 0
      scroll-step 1)
(setq comint-scroll-show-maximum-output t)

;; 1/3画面スクロール
;; カーソルは画面内固定で半画面
(defun half-page-up()
  (interactive)
  (if(= (window-end) (point-max))
      (next-line (/ (window-height) 3))
    (let ((a (current-line)))
      (if(< a 1) (setq a 1))
      (scroll-up (/ (window-height) 3))
      (move-to-window-line a)
      )))
(defun half-page-down()
  (interactive)
  (if(= (window-start) 1)
      (next-line (/ (window-height) -3))
    (let ((a (current-line)))
      (scroll-down (/ (window-height) 3))
      (move-to-window-line a)
      )))
(defun current-line()
  "Return the vertical position of point..."
  (cdr (nth 6 (posn-at-point))))
(define-key global-map (kbd "C-v") 'half-page-up)
(define-key global-map (kbd "M-v") 'half-page-down)

;; エラー音をならなくする
(setq ring-bell-function 'ignore)
;; bufferの最後でカーソルを動かそうとしても音をならなくする
(defun next-line (arg)
  (interactive "p")
  (condition-case nil
    (line-move arg)
    (end-of-buffer)))

;; バッファ自動再読み込み(外部から編集された場合に読み込まれる)
(global-auto-revert-mode 1)

;; ミニバッファの履歴を3000まで保存する
(savehist-mode 1)
(setq history-length 3000)

;; バックアップを残さない
(setq make-backup-files nil)
(setq auto-save-default nil)
(setq auto-save-list-file-prefix nil)
(setq create-lockfiles nil)

;; デフォルトの文字コードと改行コード
(set-default-coding-systems 'utf-8-unix)
;; [for windows] パスとファイル名はShift-Jis
(setq default-file-name-coding-system 'japanese-cp932-dos)

;; tabサイズ タブにスペースを使用する
(setq default-tab-width 4)
(setq-default tab-width 4 indent-tabs-mode nil)
;; 全角スペースとタブを可視化
(global-whitespace-mode 1)
(setq whitespace-space-regexp "\\(\u3000\\)")
(setq whitespace-style '(face tabs tab-mark spaces space-mark))
(setq whitespace-display-mappings
      '((space-mark ?\x3000 [?\□])
        (tab-mark   ?\t   [?\xBB ?\t])
        ))

;; 行末の空白を保存時に削除
(defvar delete-trailing-whitespece-before-save t)
(defun my-delete-trailing-whitespace ()
  (if delete-trailing-whitespece-before-save
      (delete-trailing-whitespace)))
(add-hook 'before-save-hook 'my-delete-trailing-whitespace)
;; markdown-modeの時は行末の空白を削除しない
(add-hook 'markdown-mode-hook
          '(lambda ()
             (set (make-local-variable 'delete-trailing-whitespece-before-save) nil)))

;; yes or no を y と n に変更
(defalias 'yes-or-no-p 'y-or-n-p)

;; バックスペースの設定
(global-set-key (kbd "C-h") 'delete-backward-char)

;; other-window のキーバインドを変更 direモードに対応
(global-set-key (kbd "C-t") 'other-window)
(add-hook 'dired-mode-hook (lambda ()(local-unset-key (kbd "C-t"))))

;; コメントアウトキーバインドの変更
(define-key global-map (kbd "M-:") 'comment-dwim)

;; ウィンドウ幅修正 ctrl + 矢印キー
(global-set-key [(ctrl up)] '(lambda (arg) (interactive "p") (shrink-window arg)))
(global-set-key [(ctrl down)] '(lambda (arg) (interactive "p") (shrink-window (- arg))))
(global-set-key [(ctrl right)] '(lambda (arg) (interactive "p") (shrink-window-horizontally arg)))
(global-set-key [(ctrl left)] '(lambda (arg) (interactive "p") (shrink-window-horizontally (- arg))))

;; [for mac] optionをメタキーにする
;; (setq mac-option-modifier 'meta)

;; 起動時のウィンドウサイズ・位置の変更
;;(setq initial-frame-alist
;;      (append (list
;;               '(width . 128)
;;               '(height . 50)
;;               '(top . 0)
;;               '(left . 70)
;;               )
;;              initial-frame-alist))
;;(setq default-frame-alist initial-frame-alist)

;; org-mode 設定
(org-mode)
(setq org-directory "~/.emacs.d/memos/org/")
(setq org-agenda-files (list org-directory))
(setq org-startup-with-inline-images t)
(define-key org-mode-map (kbd "C-o") 'org-open-at-point)

;; python-mode 設定
;; [for Windows]virtualenvで作成した環境のpython.exeのpath
(setq python-shell-interpreter "~/tools/pyenv/jupyterlab-env/Scripts/python.exe")
3
;; web検索エンジンのデフォルトを変更
;; 検索コマンド M-s M-w
(setq eww-search-prefix "https://www.google.co.jp/search?btnI&q=")

;;=======================================================
;; ▲ 必須の基本設定 ここまで
;;=======================================================
;; el-getでpackage管理
;; git clone https://github.com/dimitri/el-get
(package-initialize)

;; load-pathの追加関数
(defun add-to-load-path (&rest paths)
  (let (path)
    (dolist (path paths paths)
      (let ((default-directory (expand-file-name (concat user-emacs-directory path))))
        (add-to-list 'load-path default-directory)
        (if (fboundp 'normal-top-level-add-subdirs-to-load-path)
            (normal-top-level-add-subdirs-to-load-path))))))

;; load-pathに追加するフォルダ
;; 2つ以上フォルダを指定する場合の引数 => (add-to-load-path "elisp" "xxx" "xxx")
(add-to-load-path "el-get" "packages" "themes")

;; el-getの設定
;; el-getでダウンロードしたパッケージは ~/.emacs.d/packages に入るようにする
(require 'el-get)
(setq el-get-dir (locate-user-emacs-file "packages"))

;;=======================================================
;; ▼ 任意の設定
;;=======================================================
;; セットアップ時requireに指定されているものを導入

;; helm
;; M-x el-get-install RET helm
(require 'helm)
(require 'helm-config)
(helm-mode 1)
;; helm-mode呼出しキーバインド設定
(global-set-key (kbd "C-x C-f") 'helm-find-files)
(global-set-key (kbd "C-x C-b") 'helm-buffers-list)
(global-set-key (kbd "C-x b") 'helm-buffers-list)
(global-set-key (kbd "M-x") 'helm-M-x)
;; TABで補完 C-iまたはC-zでメニュー
(define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action)
(define-key helm-map (kbd "<C-tab>") 'helm-execute-persistent-action)
(define-key helm-map (kbd "C-i") 'helm-select-action)
(define-key helm-map (kbd "C-z") 'helm-select-action)
;; C-hによる1文字削除対応
(define-key helm-map (kbd "C-h") 'delete-backward-char)
(define-key helm-read-file-map (kbd "C-h") 'delete-backward-char)
(define-key helm-find-files-map (kbd "C-h") 'delete-backward-char)
;; C-kを既存と合わせた動作にする
(setq helm-delete-minibuffer-contents-from-point t)
(defadvice helm-delete-minibuffer-contents (before helm-emulate-kill-line activate)
  "Emulate `kill-line' in helm minibuffer"
  (kill-new (buffer-substring (point) (field-end))))
(setq helm-split-window-in-side-p    t ; open helm buffer inside current window, not occupy whole other window
      helm-ff-search-library-in-sexp t ; search for library in `require' and `declare-function' sexp.
      )
;; helm-descbinds
;; M-x el-get-install RET helm-descbinds
(require 'helm-descbinds)
(helm-descbinds-mode)

;; スニペット yasnippet
;; M-x el-get-install RET yasnippet
;; M-x el-get-install RET helm-c-yasnippet
(custom-set-variables '(yas-alias-to-yas/prefix-p nil))
(custom-set-variables '(yas-indent-line 'fixed))
(custom-set-variables '(yas-new-snippet-default "\
# -*- mode: snippet -*-
# name: $1
# key: ${2:${1:$(yas--key-from-desc yas-text)}}
# expand-env:
# --
$0`(yas-escape-text yas-selected-text)`"))
(require 'yasnippet)
(require 'helm-c-yasnippet)
(setq helm-yas-space-match-any-greedy t)
(global-set-key (kbd "C-u") 'helm-yas-complete)
(push '("~/.emacs.d/snippets/" . snippet-mode) auto-mode-alist)
(yas-global-mode 1)

;; オートコンプリート
;; M-x el-get-install RET auto-complete
(require 'auto-complete-config)
(ac-config-default)
(setq ac-use-menu-map t)
(setq ac-quick-help-delay 1.0)

;; Undo redo
;; M-x el-get-install RET undo-tree
(require 'undo-tree)
(global-undo-tree-mode t)

;; windows.el
;; M-x el-get-install RET windows
(require 'windows)
(setq win:use-frame nil)
(define-key global-map (kbd "<C-tab>") 'win-toggle-window)

;; smooth-scroll スクロール単位3
;; M-x el-get-install RET smooth-scroll
(require 'smooth-scroll)
(setq smooth-scroll/vscroll-step-size 3)
(smooth-scroll-mode t)

;; markdown-mode
;; M-x el-get-install RET markdown-mode
(require 'markdown-mode)
(add-to-list 'auto-mode-alist '("\\.md$" . markdown-mode))

;; smart-mode-line
;; M-x el-get-install RET smart-mode-line
;; カスタマイズは M-x sml/customize
(require 'smart-mode-line)
(setq sml/extra-filler -10)
(setq sml/no-confirm-load-theme t)
(sml/setup)
(column-number-mode t)
(line-number-mode t)

;; open-junk-file
;; M-x el-get-install RET open-junk-file
(require 'open-junk-file)
(setq open-junk-file-format "~/.emacs.d/memos/junk/%Y/%m/%Y-%m-%d-%H%M%S.")
(setq open-junk-file-find-file-function 'find-file)
(global-set-key (kbd "C-j") 'open-junk-file)
(define-key org-mode-map (kbd "C-j") 'open-junk-file)

;; ace-isearch-mode
;; M-x el-get-install RET ace-isearch-mode
(require 'ace-isearch)
(setq ace-isearch-use-ace-jump nil)
(global-ace-isearch-mode 1)

;;=======================================================
;; ▼ 起動時の操作 ウィンドウを開きまくる
;;=======================================================
;; window[1~5]作成
(win:startup-with-window)
(win:switch-window 2 nil t)
(win:switch-window 3 nil t)
(win:switch-window 4 nil t)
(win:switch-window 5 nil t)
(win:switch-window 6 nil t)
(win:switch-window 7 nil t)
(win:switch-window 8 nil t)
(win:switch-window 9 nil t)

;; window[1] shell
(win-switch-to-window 0 1)
(shell)
(delete-other-windows)

;; window[2] workspace.org
(win-switch-to-window 0 2)
(find-file "~/.emacs.d/memos/org/workspace.org")

(cd "~/")
;;=======================================================
;; テーマ1 変更 M-x customize-create-theme RET
;;=======================================================
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")
(setq custom-theme-directory "~/.emacs.d/themes")
(require 'custom_graygreen-theme)

;;=======================================================
;; テーマ2 変更 M-x list-faces-display RET
;;=======================================================
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages (quote (minimap el-get))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(helm-ff-directory ((t (:foreground "pale green"))))
 '(helm-ff-file ((t (:inherit default))))
 '(helm-selection ((t (:background "white" :distant-foreground "black"))))
 '(helm-source-header ((t (:background "#c0cfc0" :foreground "#333333" :weight bold :family "Consolas"))))
 '(sml/filename ((t (:inherit sml/global :foreground "dim gray")))))
