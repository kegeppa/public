# Products
## AI_Experiment
実験用Notobook配置場所  
## Auto Machine Learning
簡易的なDataRobotもどきをskleranで実装  
機械学習入門時に作ったもの  
## Emacs Setting
emacsの基本設定を保存  
## Global Memo
PyPIデビューするために作ったコマンドラインアプリケーション  
シェル上ですぐメモを取れる便利(？)なアプリ  
どうせならと思ってsicfwを使いました  
`pip install gmemo`でインストール可能  
[PyPIはこちら](https://pypi.org/project/gmemo/)  
## Reinforce Learning
強化学習の実装、デモ  
chainerでDQN,ActorCriticを実装  
## Search From Plot
Notebook用  
散布図や箱ひげ図上をドラッグすることで、選択範囲内のデータを抽出するツール  
外れ値や、予測を大きく外したデータなどを確認するときに便利かも  
（縦軸, 横軸）が（連続値, 連続値）または（連続値, カテゴリ値）に対応  
`pip install sfplot`でインストール可能  
[PyPIはこちら](https://pypi.org/project/sfplot/)  
## Simple Interactive Console Framework
PyPIデビューするために簡易なフレームワークを実装  
対話型アプリ作成を支援するパッケージ  
python3系で動作します。動作させるほどのものではないです  
`pip install sicfw`でインストール可能  
[PyPIはこちら](https://pypi.org/project/sicfw/)  
