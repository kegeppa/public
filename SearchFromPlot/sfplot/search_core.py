# -*- coding:utf-8 -*-
"""
座標検索のコア機能

Functions
---------
convert_out_of_range2min_or_max :
convert_drag_point2scaling_point :
convert_drag_point2scaling_point :
convert_scaling_point2data_range :
convert_scaling_point2index_range :
"""
import numpy as np
import pandas as pd
import tkinter as tk
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg


def convert_out_of_range2min_or_max(min_value, max_value, target):
    """
    最小最大の範囲外を最小または最大に変換

    Parameters
    ----------
    min_value : float
        最小値
    max_value : float
        最大値
    target : float
        変換したい値

    Returns
    -------
    target : float
        変換後の値
    """
    if target < min_value:
        return min_value
    elif max_value < target:
        return max_value
    else:
        return target


def convert_selection_range2scaling_from_to(start_point, end_point, size):
    """
    選択範囲(1次元)を座標から画面全体のパーセントにスケーリングし、順序も直す
    返り値のfrom_とto_は、from_ < to_ となる

    Parameters
    ----------
    start_point : float
        選択開始座標
    end_point : float
        選択終了座標
    size : float
        画面サイズ

    Returns
    -------
    from_ : float
        開始座標(画面サイズのパーセント)
    to_ : float
        終了座標(画面サイズのパーセント)
    """
    from_ = min([start_point, end_point]) / size
    to_   = max([start_point, end_point]) / size
    return from_, to_


def convert_drag_point2scaling_point(min_value, max_value, drag_point, drop_point):
    """
    選択範囲(1次元)の座標を画面サイズのパーセントに変換する

    min_value=100, max_value=600,drag_point=150,drop_point=200の場合、
    from_=0.1, to_=0.2を返却する

    Parameters
    ----------
    min_value : float
        画面サイズ開始座標
    max_value : float
        画面サイズの終了座標
    drag_point : float
        選択開始座標
    drop_point : float
        選択終了座標

    Returns
    -------
    from_ : float
        選択範囲の画面パーセンテージ(開始)
    to_ : float
        選択範囲の画面パーセンテージ(終了)
    """
    drag_in_range = convert_out_of_range2min_or_max(min_value, max_value, drag_point)
    drop_in_range = convert_out_of_range2min_or_max(min_value, max_value, drop_point)
    from_, to_ = convert_selection_range2scaling_from_to(drag_in_range, drop_in_range, max_value-min_value)
    return from_, to_


def convert_scaling_point2data_range(from_point, to_point, data_min, data_max):
    """
    画面パーセンテージから、データの値に変換

    Parameters
    ----------
    from_point : float
        開始座標
    to_point : float
        終了座標
    data_min : float
        画面に表示されているデータ範囲の開始値
    data_max : float
        画面に表示されているデータ範囲の終了値

    Returns
    -------
    data_range_from : float
        選択されたデータの開始値
    data_range_to : float
        選択されたデータの終了値
    """
    data_range_from = (data_max - data_min) * from_point + data_min
    data_range_to   = (data_max - data_min) * to_point   + data_min
    return data_range_from, data_range_to


def convert_scaling_point2index_range(from_point, to_point, data_num):
    """
    画面全体のパーセンテージから、選択されたカテゴリ値リストに変換

    Parameters
    ----------
    from_point : float
        開始座標
    to_point : float
        終了座標
    data_num : int
        カテゴリ数

    Returns
    -------
    data_index_from : int
        選択されたカテゴリの開始インデックス
    data_index_to : int
        選択されたカテゴリの終了インデックス
    """
    unit_size = 1 / data_num
    data_index_from = max(int(from_point // unit_size), 0)
    data_index_to   = min(int(to_point   // unit_size), data_num-1)

    return data_index_from, data_index_to
