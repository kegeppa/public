# code : utf-8
import pandas as pd
import copy
from sklearn.feature_selection import RFE
from sklearn.ensemble import RandomForestClassifier
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split, RandomizedSearchCV
from sklearn.metrics import f1_score, roc_auc_score, accuracy_score, average_precision_score, precision_score, recall_score
from sklearn.externals import joblib

ACCURACY_SCORE          = 'accuracy'
ROC_AUC_SCORE           = 'roc_auc'
F1_SCORE                = 'f1'
#AVERAGE_PRECISION_SCORE = 'average_precision'
PRECISION_SCORE         = 'precision'
RECALL_SCORE            = 'recall'

SCORES = []
SCORES.append(ACCURACY_SCORE)
SCORES.append(ROC_AUC_SCORE)
SCORES.append(F1_SCORE)
#SCORES.append(AVERAGE_PRECISION_SCORE)
SCORES.append(PRECISION_SCORE)
SCORES.append(RECALL_SCORE)

class Model :

    _step = 0.05

    _test_size = 0.25
    _random_state = 1

    def __init__(self, X, y, estimatorFactory, scoring):
        self._X = X
        self._y = y
        self._est = estimatorFactory.getEstimator()
        self._est_options = estimatorFactory.options
        self._featureSelectFlag = estimatorFactory.featureSelectFlag
        self._scoring = scoring
        self._selector = None
        self._model = None
        self.name = estimatorFactory.name
        self.scores = {}

    def _featureSelect(self, featuresNum):
        if self._featureSelectFlag == False:
            return

        if featuresNum >= len(self._X.columns.values):
            return

        #ランダムフォレストによる特徴量選択に固定
        #self._selector = RFE(estimator=self._est, n_features_to_select=featuresNum, step=self._step)
        self._selector = RFE(estimator=RandomForestClassifier(random_state=1), n_features_to_select=featuresNum, step=self._step)
        self._selector.fit(self._X,self._y.as_matrix().ravel())
        self._X = pd.DataFrame(self._selector.transform(self._X), columns=self._X.columns.values[self._selector.support_])

    def learn(self):
        # FeatureSelect
        self._featureSelect(20)
        # Modeling
        X_train, X_test, y_train, y_test = train_test_split(self._X, self._y, test_size=self._test_size, random_state=self._random_state)
        pipe = Pipeline([('scl', StandardScaler()),('est', self._est)])
        # Param Tuning , Modeling
        rs = RandomizedSearchCV(estimator=pipe, param_distributions=self._est_options, n_iter=30, scoring=self._scoring, cv=3, random_state=1)
        rs.fit(X_train, y_train.as_matrix().ravel())
        bestModel = rs.best_estimator_
        # Score
        y_predict = bestModel.predict(X_test)
        scores = {}
        scores[ACCURACY_SCORE] = accuracy_score(y_test, y_predict)
        # yに含まれるラベル数が2以下の場合、評価指標を追加
        if self._y[self._y.columns.values[0]].value_counts().count() <= 2:
            scores[ROC_AUC_SCORE] = roc_auc_score(y_test, y_predict)
            scores[F1_SCORE] = f1_score(y_test, y_predict)
            #scores[AVERAGE_PRECISION_SCORE] = average_precision_score(y_test, y_predict)
            scores[PRECISION_SCORE] = precision_score(y_test, y_predict)
            scores[RECALL_SCORE] = recall_score(y_test, y_predict)
        else:
            scores[ROC_AUC_SCORE] = 0.0
            scores[F1_SCORE] = 0.0
            #scores[AVERAGE_PRECISION_SCORE] = 0.0
            scores[PRECISION_SCORE] = 0.0
            scores[RECALL_SCORE] = 0.0
        # Save
        print('BEST MODEL: %s' % (bestModel))
        joblib.dump(bestModel, './data/saveModel/' + self.name + '.pkl')
        self.scores = scores
        self._model = bestModel

    def predict(self, id, predictX, classMapping):
        # classMapping
        classes = self._model.classes_
        reversedClassMapping = {}
        for key, value in classMapping.items():
            reversedClassMapping[value] = key

        if len(reversedClassMapping) > 0:
            reversedClasses = []
            for index in range(len(classes)):
                 reversedClasses.append(reversedClassMapping[classes[index]])
            classes = reversedClasses

        # Featureselect
        if self._selector is not None:
            predictX = predictX.loc[:, predictX.columns.values[self._selector.support_]]

        # Predict
        predict = pd.DataFrame(self._model.predict(predictX), columns=self._y.columns.values)
        if len(reversedClassMapping) > 0:
            predict.iloc[:,0] = predict.iloc[:,0].map(reversedClassMapping)
        probas = pd.DataFrame(self._model.predict_proba(predictX), columns=classes)
        if id is None:
            result = predict.join(probas)
            result_predict = predict
            result_proba = probas
        else:
            result = id.join(predict)
            result = result.join(probas)
            result_predict = id.join(predict)
            result_proba = id.join(probas)

        resultCsvPath = './data/probas/' + self.name + '.csv'
        predictCsvPath = './data/probas/' + self.name + '_predict.csv'
        probaCsvPath = './data/probas/' + self.name + '_proba.csv'
        result.to_csv(resultCsvPath, index=False)
        result_predict.to_csv(predictCsvPath, index=False)
        result_proba.to_csv(probaCsvPath, index=False)
        print(resultCsvPath)
        print(predictCsvPath)
        print(probaCsvPath)

        ##### 課題用 #####
        #forWork = probas.iloc[:,1]
        #if id is None:
        #    forWork = forWork
        #else:
        #    forWork = id.join(forWork)
        #forWork.to_csv('./data/probas/' + self.name + '_forWork.csv', index=False)
        #################

    def loadModel(self):
        self._model = joblib.load('./data/saveModel/' + self.name + '.pkl')
