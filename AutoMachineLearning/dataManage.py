# coding : utf-8

import pandas as pd
from sklearn.preprocessing import Imputer

class DataManage():

    def __init__(self):
        self._dropColumns = []
        self._oheColumns = []
        self._imp = None
        self._classMapping = {}

    def dropColumn(self, X, target):
        self._dropColumns.append(target)
        return X.drop(target, axis=1)

    def dropColumnForPredict(self, X):
        for target in self._dropColumns:
            if target in X.columns.values:
                X = X.drop(target, axis=1)
        return X

    def oneHotEncode(self, X, ohe_columns=[]):
        self._oheColumns.extend(ohe_columns)
        return pd.get_dummies(X, dummy_na=True, columns=ohe_columns)

    def oneHotEncodeForPredict(self, X):
        return pd.get_dummies(X, dummy_na=True, columns=self._oheColumns)

    def impute(self, X):
        self._imp = Imputer(missing_values='NaN', strategy='mean', axis=0)
        self._imp.fit(X)
        return pd.DataFrame(self._imp.transform(X), columns=X.columns.values)

    def imputeForPredict(self, X):
        return pd.DataFrame(self._imp.transform(X), columns=X.columns.values)

    def convertY(self, y):
        y_col_name = y.columns.values[0]
        y_col = y[y_col_name]

        if y_col.value_counts().count() != 2:
            return y

        self._classMapping = {}
        for index, values in enumerate(y_col.value_counts().index):
            self._classMapping[values] = index
            # = {y_col.value_counts().index[0]:0, y_col.value_counts().index[1]:1}
        y.loc[:, y_col_name] = y.loc[:, y_col_name].map(self._classMapping)
        return y

    def makeSameColumns(self, X, predictX):
        X_cols_m = pd.DataFrame(None, columns=X.columns.values, dtype=float)
        predictX_same_columns = pd.concat([X_cols_m, predictX])
        # 予測したいデータのみに登場するデータ項目を削除(予測リスト - モデリングリスト = 予測データのみに登場するカラム)
        diff_score = list(set(predictX.columns.values) - set(X.columns.values))
        predictX_same_columns = predictX_same_columns.drop(diff_score, axis=1)
        # モデリングのデータのみに存在するカラムのデータを0埋め
        diff_model = list(set(X.columns.values) - set(predictX.columns.values))
        predictX_same_columns.loc[:,diff_model] = predictX_same_columns.loc[:,diff_model].fillna(0, axis=1)
        # モデリングデータの並び順に合う様に予測データの並び順をなおす
        predictX_same_columns = predictX_same_columns.reindex_axis(X.columns.values, axis=1)

        return predictX_same_columns


if __name__ == '__main__':
    df = pd.read_csv('./data/test.csv')
    y = df.iloc[:, [-1]]
    print(y.head(10))
    y = convertY(y)
    print(y.head(10))
