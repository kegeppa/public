# coding : utf-8

class Mode:
    """ コンソールの振る舞いを決定するモードの親クラス
        data     : ProjectDataクラス
        nextMode : Modeクラス

        実装時はpremessageとreactionのオーバーライドが必須
    """

    def __init__(self, data):
        self.data = data
        self.nextMode = self

    def premessage(self):
        print('=======================')
        print('quit->Q ')

    def waitinput(self):
        return input('>>> ').strip()

    def reaction(self,input_text):
        print('input %s' % (input_text))

    def quitEnable(self):
        return True

    def quit(self,input_text):
        if self.quitEnable and input_text == 'Q':
            return True
        else:
            return False

    def clear(self):
        for i in range(50):
            print(' ')

class Console:
    """ コンソール機能を提供
        initialMode : Modeクラス

        Modeクラスのメソッドを順番に呼び出す
        1. premessage
        2. waitinput
        3. quit
        4. clear
        5. reaction

        quitメソッドの返り値がTrueの場合、コンソール処理を終了する
    """

    def __init__(self, initialMode):
        self._mode = initialMode

    def _console(self, mode):
        mode.premessage()
        input_text = mode.waitinput()
        if mode.quit(input_text):
            return False
        else:
            mode.clear()
            mode.reaction(input_text)
            return True

    def start(self):
        self._mode.clear()
        while True:
            if self._console(self._mode):
                self._mode = self._mode.nextMode
                continue
            else:
                break

        print(' ')
        print('bye bye')
        print(' ')

if __name__ == '__main__':
    data = None
    console = Console(Mode(data))
    console.start()
