# coding : utf-8
import pandas as pd

inputCsvPath = './data/dateConvertTest.csv'
X = pd.read_csv(inputCsvPath, header=0)
print(X.head(10))
print('')

columnName = 'Outlet_Establishment_Year'
CDATE = 2013
X.loc[:, [columnName]] = CDATE - X.loc[:, [columnName]]

outputCsvPath = './data/dateConvertTest_converted.csv'
X.to_csv(outputCsvPath, index=False)
print(X.head(10))
