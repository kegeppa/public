# coding : utf-8

import pandas as pd

class DataLoad:

    def __init__(self, path, header):
        self.csvData = pd.read_csv(path, header=header)
        self.X = None
        self.y = None

    def addCsv(self, path, header):
        data = pd.read_csv(path, header=header)
        if len(self.csvData) == len(data):
            self.csvData = pd.concat([self.csvData, data], axis=1)

    def selectYByName(self, columnName):
        self.y = self.csvData.loc[:, [columnName]]
        self.X = self.csvData.drop(columnName, axis=1)

    def selectYByIndex(self, columnIndex):
        self.y = self.csvData.iloc[:, [columnIndex]]
        columnName = self.csvData.columns.values[columnIndex]
        self.X = self.csvData.drop(columnName, axis=1)

if __name__ == '__main__' :
    path   = 'test.csv'
    header = 0

    DataSet(path, header)
