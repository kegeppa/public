# coding : utf-8

import estimatorFactory
import dataManage
import pickle

class ProjectData:

    def __init__(self):
        # プロジェクト名
        self.projectName = 'AAA'

        # 元データ
        self.csvData = None
        # 加工後のデータ
        self.X = None
        self.y = None

        # 予測データ
        self.predictID = None
        self.predictX = None

        # モード間オブジェクト引き継ぎ用
        self.dataLoadObj = None
        self.dataManageObj = dataManage.DataManage()
        self.selectedScoring = None
        self.learnedModelList = {}

        # アルゴリズムリスト
        self.classifierEstimatorFactories = estimatorFactory.getClassifierEstimatorFactories()
        self.regressionEstimatorFactories = estimatorFactory.getRegressionEstimatorFactories()

    def save(self, path):
        savePath = path + self.projectName + '.pickle'
        with open(savePath, 'wb') as f:
            pickle.dump(self, f)
        return savePath

    def load(self, path):
        projectData = None
        with open(path + self.projectName + '.pickle', 'rb') as f:
            projectData = pickle.load(f)
        return projectData


if __name__ == '__main__':
    #project = ProjectData()
    #project.dataLoadObj = 'changed'
    #project.save('./data/')
    project = ProjectData()
    print(project.dataLoadObj)
    project = project.load('./data/', 'AAA')
    print('')
    print('Loaded')
    print(project.dataLoadObj)
