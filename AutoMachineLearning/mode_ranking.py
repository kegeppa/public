# coding : utf-8
from console import Mode
import modeling
import mode_projectmenu

class Mode_ModeSelect(Mode):

    COMMAND_ALL  = 'A'
    COMMAND_BACK = 'b'

    def premessage(self):
        print('-----------------------------------------------')
        print('Ranking - select rank mode')
        print('')
        for index, scoreName in enumerate(modeling.SCORES):
            print(' %3d : %s' % (index, scoreName))
        print('')
        print(' ' + self.COMMAND_ALL + ' : All')
        print('')
        print(' ' + self.COMMAND_BACK + ' : Back to ProjectMenu')
        print('')

    def reaction(self, input_text):
        def _ranking(targetScore):
            #ランキング用ソート前リスト作成
            modelScoreList = []
            for modelName, model in self.data.learnedModelList.items():
                modelScoreList.append((model.scores[targetScore], modelName))
            #ランキングソート
            modelScoreList = sorted(modelScoreList, reverse=True)
            #出力
            print(targetScore)
            for modelScore in modelScoreList:
                print(' %s : %.6f' % (modelScore[1].ljust(25), modelScore[0]))

        if input_text == '':
            return
        elif input_text == self.COMMAND_BACK:
            self.nextMode = mode_projectmenu.Mode_ModeSelect(self.data)
        elif input_text == self.COMMAND_ALL:
            for targetScore in modeling.SCORES:
                _ranking(targetScore)
        else:
            indexList = input_text.split(' ')
            #スコア毎のランキング作成
            for index in indexList:
                targetScore = modeling.SCORES[int(index)]
                _ranking(targetScore)
