# coding : utf-8
from console import Mode
from dataLoad import DataLoad
import mode_projectmenu

class Mode_LoadCSV(Mode):

    COMMAND_ENDLOADCSV = 'e'

    def premessage(self):
        print('-----------------------------------------------')
        print('Predict - Load CSV')
        print('')
        print('input csv file path. (ex)./data/test.csv')
        print(' ' + self.COMMAND_ENDLOADCSV + ' : End load csv.')
        print('')
        print(' ')

    def reaction(self, input_text):
        if input_text ==  self.COMMAND_ENDLOADCSV:
            self.nextMode = Mode_SelectID(self.data)
        elif input_text == '':
            print('please input anything')
            print('')
        else:
            if self.data.dataLoadObj is None:
                self.data.dataLoadObj = DataLoad(input_text, 0)
            else:
                self.data.dataLoadObj.addCsv(input_text, 0)
            print(self.data.dataLoadObj.csvData.head(10))
            print('')
            print('Load Successed. Add Csv?')

class Mode_SelectID(Mode):

    COMMAND_NONE = 'n'

    def premessage(self):
        print('-----------------------------------------------')
        print('Predict - Select ID')
        print('')
        print('  index : columnName')
        for index, column in enumerate(self.data.dataLoadObj.csvData.columns.values):
            print('  %5d : %s' % (index, column))
        print('')
        print('input index or columnName')
        print('')
        print(' ' + self.COMMAND_NONE + ' : None')

    def reaction(self, input_text):
        if input_text == '':
            print('please input anything')
            print('')
            return
        elif input_text == self.COMMAND_NONE:
            self.data.dataLoadObj.y = None
            self.data.dataLoadObj.X = self.data.dataLoadObj.csvData
        elif input_text.isnumeric():
            self.data.dataLoadObj.selectYByIndex(int(input_text))
        else:
            self.data.dataLoadObj.selectYByName(input_text)

        self.data.predictID = self.data.dataLoadObj.y
        self.data.predictX = self.data.dataLoadObj.X

        self.data.dataLoadObj = None
        self.nextMode = Mode_SelectAlgorithm(self.data)

class Mode_SelectAlgorithm(Mode):

    COMMAND_ALL = 'A'
    keyList = []

    def premessage(self):
        print('-----------------------------------------------')
        print('Predict - Select algorithm')
        print('')
        print(' index : algorithmName')
        self.keyList = []
        for index, modelName in enumerate(self.data.learnedModelList.keys()):
            print(' %5d : %s' % (index, modelName))
            self.keyList.append(modelName)
        print('')
        print(' ' + self.COMMAND_ALL + ' : All')
        print('')

    def reaction(self, input_text):
        def _preprocess():
            self.data.predictX = self.data.dataManageObj.dropColumnForPredict(self.data.predictX)
            self.data.predictX = self.data.dataManageObj.oneHotEncodeForPredict(self.data.predictX)
            self.data.predictX = self.data.dataManageObj.makeSameColumns(self.data.X, self.data.predictX)
            self.data.predictX = self.data.dataManageObj.imputeForPredict(self.data.predictX)

        if input_text == '':
            return
        elif input_text == self.COMMAND_ALL:
            _preprocess()
            for model in self.data.learnedModelList.values():
                model.predict(self.data.predictID, self.data.predictX, self.data.dataManageObj._classMapping)
        else:
            _preprocess()
            indexList = input_text.split(' ')
            for index in indexList:
                model = self.data.learnedModelList[self.keyList[int(index)]]
                model.predict(self.data.predictID, self.data.predictX, self.data.dataManageObj._classMapping)

        print('CSV Saved')
        self.nextMode = mode_projectmenu.Mode_ModeSelect(self.data)
