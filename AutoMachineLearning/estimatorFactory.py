#coding : utf-8
import scipy
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.ensemble import GradientBoostingClassifier, RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import LinearSVC

class EstimatorFactory:

    def __init__(self):
        self.name = self.__class__.__name__
        self.options = {}
        self.featureSelectFlag = True

    def updateOption(optionName, optionValue):
        if optionName in self.options:
            self.options[optionName] = optionValue
            return True
        else:
            return False

    def getEstimator(self):
        return None

class LogisticRegressionFactory(EstimatorFactory):

    def __init__(self):
        super(LogisticRegressionFactory, self).__init__()
        self.name = 'Logistic'
        self.options['est__C'] = scipy.stats.uniform(0.1, 100)
        self.options['est__penalty'] = ['l1', 'l2']

    def getEstimator(self):
        return LogisticRegression(random_state=1)

class RandomForestClassifierFactory(EstimatorFactory):

    def __init__(self):
        super(RandomForestClassifierFactory, self).__init__()
        self.name = 'RandomForestClassifier'
        self.options['est__n_estimators'] = [6,8,10,12,14,16,18,20]
        self.options['est__max_features'] = ['auto', 0.5,0.6,0.7,0.8,0.9]
        self.options['est__max_depth'] = [None,10,20,30]

    def getEstimator(self):
        return RandomForestClassifier(random_state=1)

class GradientBoostingClassifierFactory(EstimatorFactory):

    def __init__(self):
        super(GradientBoostingClassifierFactory, self).__init__()
        self.name = 'GradientBoostingClassifier'
        self.options['est__loss'] = ['deviance']
        self.options['est__learning_rate'] = scipy.stats.uniform(0.1, 100)
        self.options['est__n_estimators'] = [70,80,90,100,110,120,130]
        self.options['est__max_depth'] = [3,4,5,6,7,8,9,10]

    def getEstimator(self):
        return GradientBoostingClassifier(random_state=1)

class KNeighborsClassifierFactory(EstimatorFactory):

    def __init__(self):
        super(KNeighborsClassifierFactory, self).__init__()
        self.name = 'KNeighborsClassifier'
        self.options['est__n_neighbors'] = [3,4,5,6,7,8,9,10]
        self.options['est__weights'] = ['uniform','distance']
        self.options['est__algorithm'] = ['auto','ball_tree','kd_tree','brute']
        self.options['est__leaf_size'] = [20,25,30,35,40]
        #self.options['est__p'] = [1,2]

        #self.featureSelectFlag = False

    def getEstimator(self):
        return KNeighborsClassifier()

class SGDClassifierFactory(EstimatorFactory):

    def __init__(self):
        super(SGDClassifierFactory, self).__init__()
        self.name = 'SGDClassifier'
        self.options['est__alpha'] = scipy.stats.uniform(0.0, 1.0)

    def getEstimator(self):
        return SGDClassifier(random_state=1)

class LinearSVCFactory(EstimatorFactory):
    def __init__(self):
        super(LinearSVCFactory, self).__init__()
        self.name = 'LinearSVC'
        self.options['est__C'] = scipy.stats.uniform(0.1, 100)
        self.options['est__class_weight'] = ['balanced']

    def getEstimator(self):
        return LinearSVC(random_state=1)

def getClassifierEstimatorFactories():
    lgrF  = LogisticRegressionFactory()
    rfcF  = RandomForestClassifierFactory()
    gbcF  = GradientBoostingClassifierFactory()
    kncF  = KNeighborsClassifierFactory()
    #sgdF  = SGDClassifierFactory()
    #lsvcF = LinearSVCFactory()
    CLASSIFICATION_ESTIMATORS_FACTORIES = {}
    CLASSIFICATION_ESTIMATORS_FACTORIES[lgrF.name]  = lgrF
    CLASSIFICATION_ESTIMATORS_FACTORIES[rfcF.name]  = rfcF
    CLASSIFICATION_ESTIMATORS_FACTORIES[gbcF.name]  = gbcF
    CLASSIFICATION_ESTIMATORS_FACTORIES[kncF.name]  = kncF
    #CLASSIFICATION_ESTIMATORS_FACTORIES[sgdF.name]  = sgdF
    #CLASSIFICATION_ESTIMATORS_FACTORIES[lsvcF.name] = lsvcF
    return CLASSIFICATION_ESTIMATORS_FACTORIES

def getRegressionEstimatorFactories():

    REGRESSION_ESTIMATOR_FACTORIES = {}

    return REGRESSION_ESTIMATOR_FACTORIES


if __name__ == '__main__':
    print(getClassifierEstimators())
