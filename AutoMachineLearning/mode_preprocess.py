# coding : utf-8
from console import Mode
from dataManage import DataManage
import mode_projectmenu


class Mode_ModeSelect(Mode):

    COMMAND_DROPCOLUMN     = '0'
    COMMAND_ONEHOTENCODING = '1'
    COMMAND_IMPUTE         = '2'
    COMMAND_CONVERTY       = '3'
    COMMAND_BACK           = 'b'

    def premessage(self):
        print('-----------------------------------------------')
        print('Preprocess')
        print('')
        print(' ' + self.COMMAND_DROPCOLUMN +     ' : Drop Column')
        print(' ' + self.COMMAND_ONEHOTENCODING + ' : One Hot Encoding')
        print(' ' + self.COMMAND_IMPUTE +         ' : Impute')
        print(' ' + self.COMMAND_CONVERTY +       ' : Convert y to binary')
        print('')
        print(' ' + self.COMMAND_BACK +           ' : Back to ProjectMenu')
        print('')

    def reaction(self, input_text):
        if input_text == self.COMMAND_DROPCOLUMN:
            self.nextMode = Mode_DropColumn(self.data)
        elif input_text == self.COMMAND_ONEHOTENCODING:
            self.nextMode = Mode_OneHotEncoding(self.data)
        elif input_text == self.COMMAND_IMPUTE:
            self.nextMode = Mode_Impute(self.data)
        elif input_text == self.COMMAND_CONVERTY:
            self.nextMode = Mode_ConvertY(self.data)
        elif input_text == self.COMMAND_BACK:
            self.nextMode = mode_projectmenu.Mode_ModeSelect(self.data)

class Mode_DropColumn(Mode):

    def premessage(self):
        print('-----------------------------------------------')
        print('Preprocess - Drop Column')
        print('')
        print('Select target column')
        print('')
        print('index : columnName                , columnType')
        for index, (columnName, columnType) in enumerate(zip(self.data.X.columns.values, self.data.X.dtypes)):
            print('  %3d : %s , %s' % (index, columnName.ljust(25), columnType))
        print('input any index or columnName. (ex) 0 1 columnA 6')
        print('')

    def reaction(self, input_text):
        if input_text == '':
            print('Drop column canceled.')
            self.nextMode = Mode_ModeSelect(self.data)
        else:
            targetList = input_text.split(' ')
            for target in targetList:
                if target.isnumeric():
                    target = self.data.X.columns.values[int(target)]
                self.data.X = self.data.dataManageObj.dropColumn(self.data.X, target)
            for index, columnName in enumerate(self.data.X.columns.values):
                print('  %3d : %s' % (index, columnName))
            print('Drop column succsessed.')
            self.nextMode = Mode_ModeSelect(self.data)

class Mode_OneHotEncoding(Mode):

    def premessage(self):
        print('-----------------------------------------------')
        print('Preprocess - One Hot Encoding')
        print('')
        print('Select target column')
        print('')
        print('index : columnName                , columnType')
        for index, (columnName, columnType) in enumerate(zip(self.data.X.columns.values, self.data.X.dtypes)):
            print('  %3d : %s , %s' % (index, columnName.ljust(25), columnType))
        print('input any index. (ex) 0 3 4')
        print('')

    def reaction(self, input_text):
        if input_text == '':
            print('One hot encoding canceled.')
            self.nextMode = Mode_ModeSelect(self.data)
        else:
            indexList = input_text.split(' ')
            targetList = []
            for index in indexList:
                targetList.append(self.data.X.columns.values[int(index)])
            self.data.X = self.data.dataManageObj.oneHotEncode(self.data.X, targetList)
            print('One hot encoding successed.')
            print('index : columnName')
            for index, columnName in enumerate(self.data.X.columns.values):
                print('  %3d : %s' % (index, columnName))
            self.nextMode = Mode_ModeSelect(self.data)

class Mode_Impute(Mode):

    COMMAND_YES = 'y'
    COMMAND_NO  = 'n'

    def premessage(self):
        print('-----------------------------------------------')
        print('Preprocess - Impute')
        print('')
        print('Do Impute?')
        print(' ' + self.COMMAND_YES + ' : Yes')
        print(' ' + self.COMMAND_NO  + ' : No')
        print('')

    def reaction(self, input_text):
        if input_text == self.COMMAND_YES:
            self.data.X = self.data.dataManageObj.impute(self.data.X)
            print('Impute successed.')
            self.nextMode = Mode_ModeSelect(self.data)
        else:
            print('Impute canceled.')
            self.nextMode = Mode_ModeSelect(self.data)

class Mode_ConvertY(Mode):

    COMMAND_YES = 'y'
    COMMAND_NO  = 'n'

    def premessage(self):
        print('-----------------------------------------------')
        print('Preprocess - Convert y')
        print('')
        print('-count-')
        for col_name in list(self.data.y.columns):
            print(' %s : %d' % (col_name, self.data.y[col_name].value_counts().count()))
        print('')
        print('Do convert?')
        print(' ' + self.COMMAND_YES + ' : Yes')
        print(' ' + self.COMMAND_NO  + ' : No')
        print('')

    def reaction(self, input_text):
        if input_text == self.COMMAND_YES:
            self.data.y = self.data.dataManageObj.convertY(self.data.y)
            print('Convert successed.')
            print('')
            print(self.data.dataManageObj._classMapping)
            self.nextMode = Mode_ModeSelect(self.data)
        else:
            print('Convert canceled.')
            self.nextMode = Mode_ModeSelect(self.data)
