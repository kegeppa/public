# coding : utf-8
from console import Mode
import modeling
import mode_projectmenu

class Mode_ModeSelect(Mode):

    def premessage(self):
        print('-----------------------------------------------')
        print('Modeling - select scoring')
        print('')
        print('index : scoring')
        for index, name in enumerate(modeling.SCORES):
            print(' %5d : %s' % (index, name))
        print('')
        print('input index.')
        print('')

    def reaction(self, input_text):
        if input_text == '':
            return
        else:
            self.data.selectedScoring = modeling.SCORES[int(input_text)]
            self.nextMode = Mode_AlgorithmSelect(self.data)

class Mode_AlgorithmSelect(Mode):

    COMMAND_ALL = 'A'
    keyList = []

    def premessage(self):
        print('-----------------------------------------------')
        print('Modeling - select algorithm')
        print('')
        print(' index : algorithm')
        self.keyList = []
        index = 0
        for name in self.data.classifierEstimatorFactories.keys():
            print(' %5d : %s' % (index, name))
            index = index + 1
            self.keyList.append(name)
        print('')
        print(' ' + self.COMMAND_ALL + ' : All')
        print('')
        print('input any index. (ex) 0 3 4')
        print('')

    def reaction(self, input_text):
        def _modeling(estimatorFactory):
            model = modeling.Model(self.data.X, self.data.y, estimatorFactory, self.data.selectedScoring)
            model.learn()
            self.data.learnedModelList[model.name] = model
            print(model.name)
            for score_name, score_value in model.scores.items():
                print('  %s : %.6f' % (score_name.ljust(20), score_value))

        if input_text == '':
            print('Modeling canceled.')
            self.nextMode = mode_projectmenu.Mode_ModeSelect(self.data)
        elif input_text == self.COMMAND_ALL:
            for estimatorFactory in self.data.classifierEstimatorFactories.values():
                _modeling(estimatorFactory)
            self.nextMode = mode_projectmenu.Mode_ModeSelect(self.data)
        else:
            indexList = input_text.split(' ')
            for index in indexList:
                estimatorFactory = self.data.classifierEstimatorFactories[self.keyList[int(index)]]
                _modeling(estimatorFactory)
            self.nextMode = mode_projectmenu.Mode_ModeSelect(self.data)
