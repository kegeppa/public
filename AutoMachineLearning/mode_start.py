# coding : utf-8
from console import Mode
from dataLoad import DataLoad
import mode_projectmenu

""" モード遷移
    ┃
    App_Start
    ┃
    ┣━━NewProject_LoadCsv
    ┃   ┃
    ┃  NewProject_SelectY
    ┃   ┃
    ┃  Modeling_ModeSelect
    ┃
    ┗━━

"""

class Mode_AppStart(Mode):

    COMMAND_NEW  = 'n'
    COMMAND_LOAD = 'l'

    def premessage(self):
        print('-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-')
        print('Welcome! to AML.')
        print('')
        print(' ' + self.COMMAND_NEW +  ' : New project.')
        print(' ' + self.COMMAND_LOAD + ' : Load project.')
        print(' q : Quit.')

    def reaction(self, input_text):
        if input_text == '':
            print('please input anything')
            print('')
        elif input_text == self.COMMAND_NEW:
            print('New project')
            self.nextMode = Mode_NewProject_ProjectName(self.data)
        elif input_text == self.COMMAND_LOAD:
            print('Load project')
            self.nextMode = Mode_LoadProject_ProjectName(self.data)

class Mode_NewProject_ProjectName(Mode):

    def premessage(self):
        print('-----------------------------------------------')
        print('New project - Input project name.')
        print('')
        print('input project name. (ex)AAA')
        print('')

    def reaction(self, input_text):
        if input_text == '':
            print('please input anything')
        else:
            self.data.projectName = input_text
            self.nextMode = Mode_NewProject_LoadCsv(self.data)

class Mode_NewProject_LoadCsv(Mode):

    def premessage(self):
        print('-----------------------------------------------')
        print('New project - Load CSV file.')
        print('')
        print('input csv file path. (ex)./data/test.csv')
        print('e : End load csv.')

    def reaction(self, input_text):
        if input_text ==  'e':
            self.nextMode = Mode_NewProject_SelectY(self.data)
        elif input_text == '':
            print('please input anything')
            print(' ')
        else:
            if self.data.dataLoadObj is None :
                self.data.dataLoadObj = DataLoad(input_text, 0)
            else:
                self.data.dataLoadObj.addCsv(input_text, 0)
            print(self.data.dataLoadObj.csvData.head(10))
            print('')
            print('Load Successed. Add Csv?')

class Mode_NewProject_SelectY(Mode):

    def premessage(self):
        print('-----------------------------------------------')
        print('New project - Load CSV file - select Y column index.')
        print('')
        print('  index : columnName')
        for index, column in enumerate(self.data.dataLoadObj.csvData.columns.values):
            print('  %5d : %s' % (index, column))
        print('')
        print('input index or columnName')

    def reaction(self, input_text):
        if input_text == '':
            print('please input anything')
            print('')
            return

        if input_text.isnumeric() :
            self.data.dataLoadObj.selectYByIndex(int(input_text))
        else:
            self.data.dataLoadObj.selectYByName(input_text)

        self.data.csvData = self.data.dataLoadObj.csvData
        self.data.X = self.data.dataLoadObj.X
        self.data.y = self.data.dataLoadObj.y
        self.data.dataLoadObj = None
        self.nextMode = mode_projectmenu.Mode_ModeSelect(self.data)


class Mode_LoadProject_ProjectName(Mode):

    def premessage(self):
        print('-----------------------------------------------')
        print('Load project - Input project name.')
        print('')
        print('input project name. (ex)AAA')
        print('')

    def reaction(self, input_text):
        if input_text == '':
            print('please input anything')
        else:
            self.data.projectName = input_text
            self.data = self.data.load('./project/')
            print('Load Successed. ProjectName:' + input_text)
            self.nextMode = mode_projectmenu.Mode_ModeSelect(self.data)

if __name__ == '__main__':
    from projectData import ProjectData
    from console import Console
    data = ProjectData()
    console = Console(Mode_AppStart(data))
    console.start()
#     data.X = data.dataLoadObj.X
#     data.dataLoadObj = None
#     print(data.X.head(10))
