# coding : utf-8
from console import Mode
import mode_preprocess
import mode_modeling
import mode_ranking
import mode_predict

class Mode_ModeSelect(Mode):

    COMMAND_CONFIGRATION  = 'c'
    COMMAND_DATACONFIRM   = '0'
    COMMAND_PREPROCESSING = '1'
    COMMAND_MODELING      = '2'
    COMMAND_RANKING       = '3'
    COMMAND_PREDICT       = '4'
    COMMAND_SAVE          = '5'

    def premessage(self):
        print('-----------------------------------------------')
        print('Project Menu - ' + self.data.projectName)
        print('')
#        print(' ' + self.COMMAND_CONFIGRATION +  ' : Configration')
        print(' ' + self.COMMAND_DATACONFIRM +   ' : Data Confirm')
        print(' ' + self.COMMAND_PREPROCESSING + ' : Preprocessing')
        print(' ' + self.COMMAND_MODELING +      ' : Modeling')
        print(' ' + self.COMMAND_RANKING +       ' : Ranking')
        print(' ' + self.COMMAND_PREDICT +       ' : Predict')
        print(' ' + self.COMMAND_SAVE +          ' : Save Project')
        print('')

    def reaction(self, input_text):

        if input_text == self.COMMAND_DATACONFIRM:
            print('-X---------------------------------------------')
            print(self.data.X.dtypes)
            print('-Y---------------------------------------------')
            print(self.data.y.dtypes)
            print('-Describe X------------------------------------')
            print(self.data.X.describe())
            print('-Describe y------------------------------------')
            print(self.data.y.describe())
        elif input_text == self.COMMAND_PREPROCESSING:
            self.nextMode = mode_preprocess.Mode_ModeSelect(self.data)
        elif input_text == self.COMMAND_MODELING:
            self.nextMode = mode_modeling.Mode_ModeSelect(self.data)
        elif input_text == self.COMMAND_RANKING:
            self.nextMode = mode_ranking.Mode_ModeSelect(self.data)
        elif input_text == self.COMMAND_PREDICT:
            self.nextMode = mode_predict.Mode_LoadCSV(self.data)
        elif input_text == self.COMMAND_SAVE:
            savePath = self.data.save('./project/')
            print('Save Successed. path:' + savePath)
