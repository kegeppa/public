# -*- coding: utf-8 -*-
import copy
import time
import io
import random

import chainer
import chainer.functions as F
import chainer.links as L
from chainer import Variable, cuda, serializers

import numpy as np
import pandas as pd
import gym

print(chainer.cuda.available)
print(chainer.cuda.cudnn_enabled)

# 利用するgym環境
# https://github.com/openai/gym/wiki/Pendulum-v0
# Pendulum-v0
GYM_ENV = 'Pendulum-v0'

# 学習環境
class Environment:
    
    def __init__(self):
        self.env = gym.make(GYM_ENV)
        
        # 環境情報・ハイパーパラメータの設定
        self.obs_size = self.env.observation_space.shape[0]
        self.action_size = 1
        self.gamma = 0.9
        self.hidden_size = 32
        
        self.reset()
    
    def reset(self):
        return self.env.reset()
    
    def random_action(self):
        return self.env.action_space.sample()
    
    def step(self, action):
        """ 学習に必要なデータワンセットを返す
            行動後の状態、報酬、１試行終了フラグ、情報（デバッグ用）
        """
        after_obs, reward, finished, _ = self.env.step(action)
        return after_obs, reward, finished, _

# Agent定義
class Agent:
    
    def __init__(self, env, dqn):
        self.epsilon = 0.3
        
        self.env = env
        self.dqn = dqn
        
        self._before_obs = self.env.reset()
        self._action = self.env.random_action()[0]
        self._after_obs, self._reward, self._ep_finish, _ = self.env.step([self._action])
        
        # mountain用
        self._t = 1
    
    def action(self, obs):
        # 環境情報からQ値を取得
        q = self.dqn.forward(obs)
        
        return q
    
    def learn(self, log_interval=-1, forced_end=1000):
        """ 学習ループ処理
            環境からデータを一つずつ受け取り、dqnに渡す。
            学習は、dqnにデータを渡すと行われ、学習時のlossとqを受け取る。
            学習ループは、forced_endで指定しない限り、1000行分の学習をループする。
            
            args
                log_interval : ログを出力する頻度　指定したデータ件数を読み込む毎にログ出力
                forced_end   : 指定したデータ件数を読み込んだ後、強制的に学習を終了させる
        """
        def log(log_loss, log_q):
            if log_loss is None or log_q is None:
                return
            print(  'roop:', '%07d' % roop
                  , '  time(s):', '{:4.2f}'.format(time.time() - start_time)
                  , '  loss:', '{:4.5f}'.format(float(log_loss.data))
                  , '  q_mean:', '{:4.5f}'.format(float(np.mean(log_q.data)))
                  , '  q_max:', '{:4.5f}'.format(float(np.max(log_q.data)))
                  , '  q_min:', '{:4.5f}'.format(float(np.min(log_q.data)))
                  , '  q_var:', '{:4.5f}'.format(float(np.var(log_q.data))))
        
        start_time = time.time()
        
        # 学習開始
        for roop in range(1, forced_end + 1):
            
            # dqnにデータを渡し、学習時の情報を受け取る
            loss, q = self.dqn.data_stock(self._before_obs, [self._action], self._after_obs, [self._reward], self._ep_finish)
            
            # ログ出力
            if roop % log_interval == 0:
                log(loss, q)
            
            # 次の一行を取得
            ## 環境リセット確認
            if self._ep_finish:
                self._before_obs = self.env.reset()
                self._t = 0
            else:
                self._before_obs = self._after_obs
            
            ## e-greedy
            if random.random() > self.epsilon:
                self._action = self.env.random_action()[0]
            else:
                self._action = int(self.action(np.array([self._before_obs], dtype=np.float32))[0])
            
            ## データ取得
            self._after_obs, self._reward, self._ep_finish, _ = self.env.step([self._action])
            
            self._t += 1
            
            #if self._ep_finish:
            #    print(self._reward, self._t)

# ActorCritic定義
class ActorCritic:
    
    ##############
    # パラメータ #
    ##############
    
    # ER(experience replay)を開始するまでに貯めるデータ量
    REPLAY_START_SIZE      = 1000
    
    # ERを行う頻度(指定数データを貯めるごとに実行)
    # ※データ全体を読み込む速度に影響。大きくするほど処理は早く終わるが、未学習状態になる。4倍にすると、計算時間は1/4。
    UPDATE_INTERVAL        = 1
    
    # ER時のミニバッチサイズ
    REPLAY_SIZE            = 32
    
    # Q_dash関数(評価関数)をQ関数と同じものにする頻度(指定数データを貯めるごとに実行)
    TARGET_UPDATE_INTERVAL = 100
    
    # ER用のデータの最大蓄積量(指定の数よりも多くデータを読み込んだ場合、古いデータから上書きされる)
    # ※値は実行するマシンのメモリに依存
    DATA_SIZE              = 10**5
    
    # オプティマイザのパラメータ
    LEARNING_RATE_Q = 0.03
    LEARNING_RATE_P = 0.05
    
    # Q関数
    class QFunction(chainer.Chain):

        def __init__(self, obs_size, action_size, hidden_size=100):
            W = chainer.initializers.HeNormal(scale=0.01)
            super().__init__()
            with self.init_scope():
                self.l1 = L.Linear(obs_size + action_size, hidden_size, initialW=W)
                self.l2 = L.Linear(hidden_size, hidden_size, initialW=W)
                self.l3 = L.Linear(hidden_size, hidden_size, initialW=W)
                self.l4 = L.Linear(hidden_size, 1, initialW=W)
   
        def __call__(self, x):
            h1 = F.relu(self.l1(x))
            h2 = F.relu(self.l2(h1))
            h3 = F.relu(self.l3(h2))
            return self.l4(h3)

    # Policy関数
    class PFunction(chainer.Chain):

        def __init__(self, obs_size, action_size, hidden_size=100):
            W = chainer.initializers.HeNormal(scale=0.01)
            super().__init__()
            with self.init_scope():
                self.l1 = L.Linear(obs_size, hidden_size, initialW=W)
                self.l2 = L.Linear(hidden_size, hidden_size, initialW=W)
                self.l3 = L.Linear(hidden_size, action_size, initialW=W)
   
        def __call__(self, x):
            h1 = F.relu(self.l1(x))
            h2 = F.relu(self.l2(h1))
            return F.tanh(self.l3(h2)) * 2
    
    def __init__(self, obs_size, action_size, gamma=0.98, hidden_size=100):
        
        self.obs_size = obs_size
        self.action_size = action_size
        self._gamma = gamma
        
        # QP関数と評価関数を生成
        self.model_Q = ActorCritic.QFunction(self.obs_size, self.action_size, hidden_size).to_gpu(0)
        self.model_P = ActorCritic.PFunction(self.obs_size, self.action_size, hidden_size).to_gpu(0)
        self._target_model_update()
        
        # オプティマイザの設定
        self.optimizer_Q = chainer.optimizers.SGD(lr=ActorCritic.LEARNING_RATE_Q)
        self.optimizer_Q.setup(self.model_Q)
        self.optimizer_P = chainer.optimizers.SGD(lr=ActorCritic.LEARNING_RATE_P)
        self.optimizer_P.setup(self.model_P)
        
        # ER用のデータを蓄積するためのメモリを確保
        self.learn_data = [np.zeros((ActorCritic.DATA_SIZE, self.obs_size), dtype=np.float32), # learn_data[0] : before_obs
                           np.zeros((ActorCritic.DATA_SIZE, 1), dtype=np.float32),               # learn_data[1] : action
                           np.zeros((ActorCritic.DATA_SIZE, self.obs_size), dtype=np.float32), # learn_data[2] : after_obs
                           np.zeros((ActorCritic.DATA_SIZE, 1), dtype=np.float32),             # learn_data[3] : reward
                           np.zeros((ActorCritic.DATA_SIZE, 1), dtype=np.float32)]             # learn_data[4] : finish_one
        
        # data_stock時の動作制御用変数
        self._data_stock_count = 0    # 蓄積するデータの格納先インデックス
        self._full_stock_flag = False # メモリいっぱいにデータが入っているか判定するフラグ
        
        # ログ用変数
        self._loss = None
        self._q = None
    
    def forward(self, before_obs):
        """ forward処理
            状況に応じた、各行動におけるQ値を返す
            args
                before_obs : [[obs1],[obs2],...]
            return
                [[obs1_Q],[obs2_Q],...]
        """
        obs = Variable(cuda.to_gpu(before_obs))
        P = self.model_P(obs)
        return F.clip(P, -2.0, 2.0).data
    
    def data_stock(self, before_obs_one, action_one, after_obs_one, reward_one, finish_one):
        
        # データを溜める
        self._stock_experience(before_obs_one, action_one, after_obs_one, reward_one, finish_one)
        
        # ER実行
        if self._data_stock_count % ActorCritic.UPDATE_INTERVAL == 0:
            self._loss, self._q = self._experience_replay()
        
        # 評価関数を更新
        if self._data_stock_count % ActorCritic.TARGET_UPDATE_INTERVAL == 0:
            self._target_model_update()
            
        return self._loss, self._q
    
    def _stock_experience(self, before_obs_one, action_one, after_obs_one, reward_one, finish_one):
        """ 学習用データをため込む
            引数は全て1行分のデータ
        """
        # １行分のデータをER蓄積用のメモリに保存
        self.learn_data[0][self._data_stock_count] = before_obs_one
        self.learn_data[1][self._data_stock_count] = action_one
        self.learn_data[2][self._data_stock_count] = after_obs_one
        self.learn_data[3][self._data_stock_count] = reward_one
        self.learn_data[4][self._data_stock_count] = finish_one
        
        # ER蓄積用のメモリがいっぱいになった場合、フラグを更新
        if not self._full_stock_flag and self._data_stock_count + 1 >= ActorCritic.DATA_SIZE:
            self._full_stock_flag = True
        
        # 次に蓄積するデータの格納先インデックスをインクリメント
        self._data_stock_count = (self._data_stock_count + 1) % ActorCritic.DATA_SIZE
        
    def _experience_replay(self):
        
        # 十分な量のデータを蓄積していない場合は学習しない
        if not self._full_stock_flag and self._data_stock_count < ActorCritic.REPLAY_START_SIZE:
            return None, None
        
        # 学習対象にするデータをランダムに取り出す
        # メモリいっぱいにデータが入っている場合は、メモリ全体からランダムにデータを取りだす
        # メモリの一部のみにデータが入っている場合は、データが存在するメモリからランダムにデータを取り出す
        if self._full_stock_flag:
            max_index = ActorCritic.DATA_SIZE
        else:
            max_index = self._data_stock_count
        
        # 取り出す対象のインデックスを決定
        replay_index = np.random.choice(np.arange(max_index), size=ActorCritic.REPLAY_SIZE, replace=False)
        
        # ER実行用のメモリを確保
        before_obs_replay = np.ndarray(shape=(ActorCritic.REPLAY_SIZE, self.obs_size), dtype=np.float32)
        action_replay = np.ndarray(shape=(ActorCritic.REPLAY_SIZE, 1), dtype=np.float32)
        after_obs_replay = np.ndarray(shape=(ActorCritic.REPLAY_SIZE, self.obs_size), dtype=np.float32)
        reward_replay = np.ndarray(shape=(ActorCritic.REPLAY_SIZE, 1), dtype=np.float32)
        finish_replay = np.ndarray(shape=(ActorCritic.REPLAY_SIZE, 1), dtype=np.float32)
        # 対象のデータをER蓄積用のメモリからER実行用のメモリにコピーする
        for i in range(ActorCritic.REPLAY_SIZE):
            before_obs_replay[i] = np.asarray(self.learn_data[0][replay_index[i]], np.float32)
            action_replay[i] = self.learn_data[1][replay_index[i]][0]
            after_obs_replay[i] = np.asarray(self.learn_data[2][replay_index[i]], np.float32)
            reward_replay[i] = self.learn_data[3][replay_index[i]][0]
            finish_replay[i] = self.learn_data[4][replay_index[i]][0]
        
        # backward
        loss, Q = self._backward(before_obs_replay, action_replay, after_obs_replay, reward_replay, finish_replay)
        
        # 誤差とQ値を返す
        return loss, Q
    
    def _backward(self, before_obs, action, after_obs, reward, finish):
        """ backward処理
            引数は全てミニバッチデータ
            args
                before_obs : [[obs_1],[obs_2],...]
                action     : [action_1,action_2,...]
                after_obs  : [[obs_1],[obs_2],...]
                reward     : [reward_1,reward_2,...]
                finish     : [finish_1,finish_2,...]
            return 
                loss, Q
        """
        def actor_critic_net(obs, action=None):
            if action is None:
                _action = self.model_P(Variable(cuda.to_gpu(obs)))
            else:
                _action = Variable(cuda.to_gpu(action))
            q_obs = Variable(cuda.to_gpu(obs))
            Q = self.model_Q(F.concat((q_obs, _action), axis=1))
            
            ####
            if self._data_stock_count % 1000 == 0:
                print(_action.data)
            ####
            return Q
    
        def actor_critic_target(obs, reward, finish, num_of_batch):
            _action = self.model_target_P(Variable(cuda.to_gpu(obs)))
            q_obs = Variable(cuda.to_gpu(obs))
            Q = self.model_target_Q(F.concat((q_obs, _action), axis=1))
            target = cuda.to_gpu(reward) + self._gamma * (1-cuda.to_gpu(finish)) * Q.data
            return target
        
        # mini_batchサイズを取得
        num_of_batch = before_obs.shape[0]
        
        # 演算を行う
        
        # Q backproba
        q_Q = actor_critic_net(before_obs, action)
        q_target = actor_critic_target(after_obs, reward, finish, num_of_batch)
        q_loss = F.mean(F.huber_loss(q_Q, q_target, delta=1.0, reduce='sum_along_second_axis'))
        self.model_Q.cleargrads()
        q_loss.backward()
        self.optimizer_Q.update()
        
        # P backproba
        p_Q = actor_critic_net(before_obs)
        p_loss = F.mean(p_Q) * -1
        self.model_P.cleargrads()
        p_loss.backward()
        self.optimizer_P.update()
        
        return q_loss, q_Q
    
    def _target_model_update(self):
        self.model_target_Q = copy.deepcopy(self.model_Q)
        self.model_target_P = copy.deepcopy(self.model_P)
    
    def save(self, file_name_prefix='dqn'):
        serializers.save_npz(file_name_prefix+'_model_Q', self.model_Q)
        serializers.save_npz(file_name_prefix+'_optimizer_Q', self.optimizer_Q)
        serializers.save_npz(file_name_prefix+'_model_P', self.model_P)
        serializers.save_npz(file_name_prefix+'_optimizer_P', self.optimizer_P)
        
    def load(self, file_name_prefix='dqn'):
        serializers.load_npz(file_name_prefix+'_model_Q', self.model_Q)
        #serializers.load_npz(file_name_prefix+'_optimizer_Q', self.optimizer_Q)
        self.optimizer_Q.setup(self.model_Q)
        serializers.load_npz(file_name_prefix+'_model_P', self.model_P)
        #serializers.load_npz(file_name_prefix+'_optimizer_P', self.optimizer_P)
        self.optimizer_P.setup(self.model_P)
        self._target_model_update()

# テスト用関数
def test(clazz=None):
    
    testenv = Environment()
    testdqn = clazz(testenv.obs_size, testenv.action_size, testenv.gamma, testenv.hidden_size)
    testdqn.load()
    testagent = Agent(testenv, testdqn)

    env = gym.make(GYM_ENV)

    for i in range(3):
        obs = env.reset()
        done = False
        R = 0
        t = 0
        while not done and t < 200:
            action = int(testagent.action(np.array([obs], dtype=np.float32))[0])
            obs, r, done, _ = env.step([action])
            R += r
            t += 1
        print('test episode:', i, 'R:', R)

############################
# 学習実行
############################

# 利用するアルゴリズム
# DQN/ActorCritic
algorithm = ActorCritic

# 初期化処理を行う場合はTrue 学習を再開させる場合はFalse
initialize_processing = True

# 学習を反復させる回数
num_of_epoch = 30

############################
# 以下、学習処理
############################
env = Environment()
dqn = algorithm(env.obs_size, env.action_size, env.gamma, env.hidden_size)

if initialize_processing:
    pass
else:
    dqn.load()
    print('model loaded')

agent = Agent(env, dqn)

for i in range(num_of_epoch):
    agent.learn(log_interval=1000, forced_end=2000)
    agent.dqn.save()
    print('learn epoch count:', i + 1)
    test(clazz=algorithm)

############################
# 学習結果確認
############################
from pyvirtualdisplay import Display
display = Display(visible=0, size=(1024, 768))
display.start()
import os
os.environ["DISPLAY"] = ":" + str(display.display) + "." + str(display.screen)

env = gym.make(GYM_ENV)
testenv = Environment()
testdqn = ActorCritic(testenv.obs_size, testenv.action_size, testenv.gamma, testenv.hidden_size)
testdqn.load()
testagent = Agent(testenv, testdqn)

frames = []
for i in range(3):
    obs = env.reset()
    done = False
    R = 0
    t = 0
    while not done and t < 200:
        frames.append(env.render(mode = 'rgb_array'))
        action = int(testagent.action(np.array([obs], dtype=np.float32))[0])
        obs, r, done, _ = env.step([action])
        R += r
        t += 1
    print('test episode:', i, 'R:', R)
env.render()

import matplotlib.pyplot as plt
import matplotlib.animation
import numpy as np
from IPython.display import HTML

plt.figure(figsize=(frames[0].shape[1] / 72.0, frames[0].shape[0] / 72.0), dpi = 72)
patch = plt.imshow(frames[0])
plt.axis('off')
animate = lambda i: patch.set_data(frames[i])
ani = matplotlib.animation.FuncAnimation(plt.gcf(), animate, frames=len(frames), interval = 50)
HTML(ani.to_jshtml())