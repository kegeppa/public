#!/bin/sh
chown -R mysql:mysql /var/lib/mysql /var/run/mysqld
chmod 777 /var/run/mysqld
service mysql start
exec "$@"
